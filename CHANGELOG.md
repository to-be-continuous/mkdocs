# [2.8.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.7.0...2.8.0) (2025-01-27)


### Features

* disable tracking service by default ([1c4ce33](https://gitlab.com/to-be-continuous/mkdocs/commit/1c4ce33b8d6b5f0db9dd3c251c9fb576aff55de5))

# [2.7.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.6.1...2.7.0) (2024-11-08)


### Features

* auto configure mkdocs file ([252b0b4](https://gitlab.com/to-be-continuous/mkdocs/commit/252b0b462f924652d5b4581b7dc0f0252d9e214e))

## [2.6.1](https://gitlab.com/to-be-continuous/mkdocs/compare/2.6.0...2.6.1) (2024-09-16)


### Bug Fixes

* **image:** use squidfunk image instead of unmaintained polinux ([c8f2f6a](https://gitlab.com/to-be-continuous/mkdocs/commit/c8f2f6afc4abaf4c6031681785f3a185614120a2))

# [2.6.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.5.1...2.6.0) (2024-08-30)


### Features

* standard TBC secrets decoding ([ec2c397](https://gitlab.com/to-be-continuous/mkdocs/commit/ec2c39765b1e431e0af30b85ff0d4d97149404f1))

## [2.5.1](https://gitlab.com/to-be-continuous/mkdocs/compare/2.5.0...2.5.1) (2024-05-05)


### Bug Fixes

* **workflow:** disable MR pipeline from prod & integ branches ([bc3412d](https://gitlab.com/to-be-continuous/mkdocs/commit/bc3412d13d544e98c7f2957e52863b41198e740b))

# [2.5.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.4.0...2.5.0) (2024-04-05)


### Features

* **pages:** compress text resources before pushing to GitLab pages ([6837963](https://gitlab.com/to-be-continuous/mkdocs/commit/6837963ad1f7691bf5633740c66c985b87e712aa)), closes [#17](https://gitlab.com/to-be-continuous/mkdocs/issues/17)

# [2.4.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.3.1...2.4.0) (2024-1-27)


### Features

* migrate to CI/CD component ([c69b1e8](https://gitlab.com/to-be-continuous/mkdocs/commit/c69b1e8bb62fb6339b12cfae09f135104f4aec95))

## [2.3.1](https://gitlab.com/to-be-continuous/mkdocs/compare/2.3.0...2.3.1) (2023-12-14)


### Bug Fixes

* fix duplicate reference conflict in script ([e27ae77](https://gitlab.com/to-be-continuous/mkdocs/commit/e27ae77b14dd2839707dc6bd74338143781a6fb3))

# [2.3.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.2.1...2.3.0) (2023-12-8)


### Features

* use centralized tracking image (gitlab.com) ([27c0576](https://gitlab.com/to-be-continuous/mkdocs/commit/27c05764a221d41b8c13c6ac0a12d2db82980637))

## [2.2.1](https://gitlab.com/to-be-continuous/mkdocs/compare/2.2.0...2.2.1) (2023-10-16)


### Bug Fixes

* declare all TBC stages ([66cd31f](https://gitlab.com/to-be-continuous/mkdocs/commit/66cd31fc8880ac0e3d9f08e92ce9675f83957053))

# [2.2.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.1.0...2.2.0) (2023-05-27)


### Features

* **workflow:** extend (skip ci) feature ([b5603e4](https://gitlab.com/to-be-continuous/mkdocs/commit/b5603e4f48a9335c5882d5138dd51ff2feb729af))

# [2.1.0](https://gitlab.com/to-be-continuous/mkdocs/compare/2.0.1...2.1.0) (2023-03-21)


### Features

* **link-checker:** add links checker with lychee ([35fba5d](https://gitlab.com/to-be-continuous/mkdocs/commit/35fba5d23168dee215762e09536ea1453b85c27c))

## [2.0.1](https://gitlab.com/to-be-continuous/mkdocs/compare/2.0.0...2.0.1) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([9ba6a89](https://gitlab.com/to-be-continuous/mkdocs/commit/9ba6a89981ac05181f47c5f71ce2cff48fc88eef))

# [2.0.0](https://gitlab.com/to-be-continuous/mkdocs/compare/1.5.0...2.0.0) (2022-08-05)


### Features

* make MR pipeline the default workflow ([f7e568c](https://gitlab.com/to-be-continuous/mkdocs/commit/f7e568cdc35c8a2043aaed4efd0240d8d57e41c7))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [1.5.0](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.3...1.5.0) (2022-05-01)


### Features

* configurable tracking image ([3409cb7](https://gitlab.com/to-be-continuous/mkdocs/commit/3409cb796725f988dc7f4507d8f4d051372e3e76))

## [1.4.3](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.2...1.4.3) (2021-11-18)


### Bug Fixes

* **requirements:** multiple requirements in var ([e1e01c3](https://gitlab.com/to-be-continuous/mkdocs/commit/e1e01c314716de1735023558b57d6ba8aef9ab53))

## [1.4.2](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.1...1.4.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([eb8f82d](https://gitlab.com/to-be-continuous/mkdocs/commit/eb8f82dc374b4deee1941e01f5d51be1f7e0b478))

## [1.4.1](https://gitlab.com/to-be-continuous/mkdocs/compare/1.4.0...1.4.1) (2021-09-08)

### Bug Fixes

* change variable behaviour ([e4eea17](https://gitlab.com/to-be-continuous/mkdocs/commit/e4eea17816f3081c5b0c68384d857dbd471d99ce))

## [1.4.0](https://gitlab.com/to-be-continuous/mkdocs/compare/1.3.0...1.4.0) (2021-06-10)

### Features

* move group ([1b42366](https://gitlab.com/to-be-continuous/mkdocs/commit/1b42366c3d2479b25ba22d99d6c2b2cacd136be2))

## [1.3.0](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.2.0...1.3.0) (2021-05-20)

### Features

* **pages:** declare the environment for pages job ([3f27b3c](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/3f27b3cf1fb6d8f7812da56141124ff84046171f))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.1.1...1.2.0) (2021-05-18)

### Features

* add scoped variables support ([1b6db00](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/1b6db00827b6f7829ce7c3ac2af3ceaa07701a5e))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.1.0...1.1.1) (2021-05-17)

### Bug Fixes

* wrong cache scope ([8a39efa](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/8a39efad2ee173e1e0b95ba68021fde0bb04f40b))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/compare/1.0.0...1.1.0) (2021-05-07)

### Bug Fixes

* **pages:** handle mkdocs output folder to already be 'public' ([2e12aa1](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/2e12aa11478ed09318745170561f250b7e40d92d))

### Features

* **stage:** moving pages job to production stage ([2d90389](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/2d9038958a324d189993135c77af9300673d7530))

## 1.0.0 (2021-05-06)

### Features

* initial release ([1827646](https://gitlab.com/Orange-OpenSource/tbc/mkdocs/commit/18276469c82a8764d5f0b89251eaa4b2f8e976d5))
